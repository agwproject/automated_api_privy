module.exports = {
  global: { // Global config, shared across enviroment
    basicAuth: {
      username: 'developer', // value from .env file
      password: 'GzIyEbbHXlSftxa6EuXB', // value from .env file
      merchantKey: '8MqHNC8mzBoXVv63klK0', // value from .env file
    }
  },
  // development: {
  //   baseURL: 'https://example.com development',
  //   login  : {
  //     user: 'username development',
  //     pass: 'password',
  //   }
  // },
  development: { baseURL: '192.168.50.15:8000' },
  staging: { 
    username: 'developer', // value from .env file
    password: 'GzIyEbbHXlSftxa6EuXB', // value from .env file
    merchantKey: '8MqHNC8mzBoXVv63klK0', // value from .env file
    baseURL: 'https://api-sandbox.privy.id',
    userToken: {
      invalid: '1e65d5fcd6ab385d87eac6d459ab08b61b4e7b9ce29b253e6ac3da474e6f23df',
      rejected: 'b188aab25e5e15153124009794bb36f96480e0b52f4c8fc0fbfdf688e26d8960',
      verified: 'e3519e41035c782e67a598c9915c7387da612d849d73f6a48b20e195315deee5',
      registered: '688d1a1d9ad91643a94ac7624756d38972f3be6fd221c5124e432c7cb12f5389'
    },
    docToken: {
      completed: 'ee118e92741e9f75dd9d7a1f57819289df88eab68d8e97a68a933d8b9879e5aa'
    }
  },
  production: { baseURL: 'https://example.com production' },
}
