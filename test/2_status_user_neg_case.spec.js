const path    = require('path')
const expect  = require('chai').expect
const request = require('supertest')
const faker   = require('faker/locale/id_ID')
const config  = require('./utils/config')
const storage = require('./utils/storage')
const error   = require('./utils/error')

const agent = request(config.baseURL)

describe('Negative case for status user merchant', () => {

  var testsAuth = [
    { username: '', password:'', desc: 'with empty basic auth'},
    { username: 'abcdef', password:'abcdef', desc: 'with invalid/random username password for authorization'}
  ]
  
  var testMerchantKey = [
    { merchantKey: '', desc: 'with empty Merchant-Key'},
    { merchantKey: 'abcdef', desc: 'with invalid/random Merchant-Key'}
  ]

  var testUserToken = [
    { userToken: '', desc: 'with empty user token', cond: 'empty'},
    { userToken: 'abcdef', desc: 'with invalid/random user token', cond: 'random'},
    { userToken: '2b86b7ddbcc0da99bc1a0772a8d8baf124bbbfd6a191007b7814b2e9d7a23909', desc: 'with user token other merchant', cond: 'other'}
  ]

  testsAuth.forEach(function(test) {
    it('As a user, I couldn\'t check user status '+ test.desc, () => {
      return agent
      .post('/v3/merchant/registration/status')
      .auth(test.username, test.password)
      .set('Merchant-Key', config.merchantKey)
      .field('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Im1haWxAY2VrLmNvbSIsImV4cCI6MTU1NDMwNzE4MX0.PSEIerVncawDZshtgz8Fa0OgoDBBvpVCSYefHcF5R98')
      .expect(401)
      .then((res) => {
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message') 
        expect(res.body.message).to.eql('Unauthorized. Invalid username or password.')
      })
    })
  })

  testMerchantKey.forEach(function(test) {
    it('As a user, I couldn\'t check user status '+ test.desc, () => {
      return agent
      .post('/v3/merchant/registration/status')
      .auth(config.username, config.password)
      .set('Merchant-Key', test.merchantKey)
      .field('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Im1haWxAY2VrLmNvbSIsImV4cCI6MTU1NDMwNzE4MX0.PSEIerVncawDZshtgz8Fa0OgoDBBvpVCSYefHcF5R98')
      .expect(401)
      .then((res) => {
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.message).to.eql('Unauthorized. Invalid Merchant Key.')
      })
    })
  })
  testUserToken.forEach(function(test) {
    it('As a user, I couldn\'t check user status '+ test.desc, () => {
      return agent
      .post('/v3/merchant/registration/status')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('token', test.userToken)
      .then((res) => {
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        if(test.cond === 'empty') { 
          expect(res.body.code).to.eql(422)
          expect(res.body.errors[0].messages[0]).to.eql('cannot be blank')
        } else if(test.cond === 'random' || 'other') {
          expect(res.body.code).to.eql(404)
          expect(res.body.message).to.eql('Unable to find userToken '+ test.userToken)
        }
      })
    })
  })
})
