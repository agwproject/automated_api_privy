const path    = require('path')
const expect  = require('chai').expect
const request = require('supertest')
const faker   = require('faker/locale/id_ID')
const config  = require('./utils/config')
const storage = require('./utils/storage')
const error   = require('./utils/error')

const agent = request(config.baseURL)
const phoneHead = ['081', '083', '085','087', '089']

const dataRegistrant = { 
    email: faker.internet.email(), 
    phone: phoneHead[Math.floor(Math.random() * phoneHead.length)] + Math.floor(100000000 + Math.random() * 900000000),
    tokenUserverified: '',
    tokenUserregistered: '',
    tokenUserrejected: '',
    tokenDocumentingprogress: '',
    tokenDocumentcompleted: '',
    ktp: path.resolve(__dirname, './assets/ktp.png'),
    selfie: path.resolve(__dirname, './assets/selfie.png'),
    identity: JSON.stringify({ nik: '3404082210930002', nama: faker.name.firstName(), tanggalLahir: '1990-01-01' })
 }

 describe('Negative case for registration api', () => {
 
  var testsAuth = [
   { username: '', password:'', desc: 'with empty basic auth'},
   { username: 'abcdef', password:'abcdef', desc: 'with invalid/random username password for authorization'}
 ]

 var testMerchantKey = [
   { merchantKey: '', desc: 'with empty Merchant-Key'},
   { merchantKey: 'abcdef', desc: 'with invalid/random Merchant-Key'}
 ]

 var testsEmpty = [
   { reqField: 'email', defValue: dataRegistrant['email']},
   { reqField: 'phone', defValue: dataRegistrant['phone']},
   { reqField: 'identity', defValue: dataRegistrant['identity']},
   { reqField: 'ktp', defValue: dataRegistrant['ktp']},
   { reqField: 'selfie', defValue: dataRegistrant['selfie']}
 ] 

 var testsWithout = [
   { reqField: ['emptyfield', 'phone', 'identity', 'ktp', 'selfie'], woField: 'email'},
   { reqField: ['email', 'emptyfield', 'identity', 'ktp', 'selfie'], woField: 'phone'},
   { reqField: ['email', 'phone', 'emptyfield', 'ktp', 'selfie'], woField: 'identity'},
   { reqField: ['email', 'phone', 'identity', 'emptyfield', 'selfie'], woField: 'ktp'},
   { reqField: ['email', 'phone', 'identity', 'ktp', 'emptyfield'], woField: 'selfie'}
 ] 

 var testEmail = [
   { email: '41@d11s1fa1.ca', status: 'waiting'},
   { email: 'Harjo65@gmail.co.id', status: 'verified'},
   { email: 'testing12@gmail.com', status: 'registered'}
 ]

 var testPhone = [
   { phone: '+62812345678921', status: 'waiting'},
   { phone: '+6282135584971', status: 'verified'},
   { phone: '+62814921303591', status: 'registered'}
 ]
 
 var testPhoneLength = [
  { phone: '087792004', desc: 'less than 10digit'},
  { phone: '08779200410234', desc: 'more than 13digit'}
 ] 
 
 var testsNonImg = [
  { reqField: 'ktp', fieldValue: 'nonimg'},
  { reqField: 'selfie', fieldValue: 'nonimg'}
 ] 

 var testsIdentityNIK = [
  { fieldValue: ' ', desc: 'is empty', cond: 'blank'},
  { fieldValue: 'abasddsasd', desc: 'is not a number', cond: 'nan'},
  { fieldValue: '340408221093000', desc: 'less than 16digits', cond: 'less'},
  { fieldValue: '34040822109300022', desc: 'more than 16digits', cond: 'more'},
 ] 

 testsAuth.forEach(function(test) {
  it('As a user, I couldn\'t register '+ test.desc, () => {
     return agent
     .post('/v3/merchant/registration')
     .auth(test.username, test.password)
     .set('Merchant-Key', config.merchantKey)
     .expect(401) 
     .then((res) => {
       expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message') 
       expect(res.body.message).to.eql('Unauthorized. Invalid username or password.')  
     })
  })
 })
 testMerchantKey.forEach(function(test) {
  it('As a user, I couldn\'t register with invalid/random Merchant-Key', () => {
     return agent
     .post('/v3/merchant/registration')
     .auth(config.username, config.password)
     .set('Merchant-Key', 'merchantkey')
     .expect(401)   
     .then((res) => {
       expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
       expect(res.body.errors).to.be.an('array').that.is.empty
       expect(res.body.message).to.eql('Unauthorized. Invalid Merchant Key.')  
     })
  })
 })
 testsWithout.forEach(function(test) {
  it('As a user, I couldn\'t register without field '+ test.woField, function() {
       
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field(test.reqField[0], dataRegistrant['email'])
    .field(test.reqField[1], dataRegistrant['phone'])
    .field(test.reqField[2], dataRegistrant['identity'])
    .attach(test.reqField[3], dataRegistrant['ktp'])
    .attach(test.reqField[4], dataRegistrant['selfie'])
    .expect(422)   
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
      expect(res.body.errors[0].field).to.eql(test.woField)
    })
  })
 })
 testsEmpty.forEach(function(test) {
  it('As a user, I couldn\'t register with value of field '+ test.reqField +' empty', function() {
    var emptyField = test.reqField
    dataRegistrant[emptyField] = ''
    
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', dataRegistrant['email'])
    .field('phone', dataRegistrant['phone'])
    .field('identity', dataRegistrant['identity'])
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(422)   
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
      expect(res.body.errors[0].field).to.eql(emptyField)
      dataRegistrant[emptyField] = test.defValue
    })
  })
 })
 it('As a user, I couldn\'t register with wrong format email', () => {
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', "ainul@gmail@gmail.com")
    .field('phone', dataRegistrant.phone)
    .field('identity', JSON.stringify({ 
      nik: dataRegistrant.nik,
      nama: dataRegistrant.nama,
      tanggalLahir: dataRegistrant.tanggalLahir
    }))
    .attach('ktp', path.resolve(__dirname, './assets/ktp.png'))
    .attach('selfie', path.resolve(__dirname, './assets/selfie.png'))
    .expect(422)   
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      expect(res.body.errors[0].messages[0]).that.includes('is invalid')
    })
 })
 testEmail.forEach(function(test) {
  it('As a user, I couldn\'t register with '+ test.status +' email', function() {
    
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', test.email)
    .field('phone', dataRegistrant['phone'])
    .field('identity', dataRegistrant['identity'])
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(422) 
    .then((res) => {
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      if (test.status == 'waiting'){
        expect(res.body.errors[0].messages[0]).to.eql('Email ' + test.email  + ' has been registered, is still a verification process')
      } else {
        expect(res.body.errors[0].messages[0]).to.includes('Email ' + test.email +' already registered')
      }
    })
  })
 })
 it('As a user, I couldn\'t register with wrong format phone', () => {
  return agent
  .post('/v3/merchant/registration')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('email', dataRegistrant.email)
  .field('phone', "13431452124")
  .field('identity', JSON.stringify({ 
    nik: "3404082210930002",
    nama: dataRegistrant.nama,
    tanggalLahir: dataRegistrant.tanggalLahir
  }))
  .attach('ktp', path.resolve(__dirname, './assets/ktp.png'))
  .attach('selfie', path.resolve(__dirname, './assets/selfie.png'))
  .expect(422)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
    expect(res.body.errors[0].messages[0]).that.includes('invalid phone number')
  })
 })
 testPhone.forEach(function(test) {
  it('As a user, I couldn\'t register with '+ test.status +' phone number', function() {
    
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', dataRegistrant['email'])
    .field('phone', test.phone)
    .field('identity', dataRegistrant['identity'])
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(422)
    .then((res) => {
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      if (test.status == 'waiting'){
        expect(res.body.errors[0].messages[0]).to.eql('Phone ' + test.phone  + ' has been registered, is still a verification process')
      } else {
        expect(res.body.errors[0].messages[0]).to.eql('Phone ' + test.phone +' already registered')
      }
    })
  })
 })
 testPhoneLength.forEach(function(test) {
  it('As a user, I couldn\'t register with '+ test.desc +' phone number', function() {
    
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', dataRegistrant['email'])
    .field('phone', test.phone)
    .field('identity', dataRegistrant['identity'])
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(422)
    .then((res) => {
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
      expect(res.body.errors[0].messages[0]).to.eql('invalid phone number')     
      expect(res.body.message).to.eql('Validation(s) Error')
    })
  })
 })
 testsNonImg.forEach(function(test) {
  it('As a user, I couldn\'t register with value of field '+ test.reqField +' empty', function() {
    dataRegistrant[test.reqField] = path.resolve(__dirname, './assets/tes.pdf')
    
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', dataRegistrant['email'])
    .field('phone', dataRegistrant['phone'])
    .field('identity', dataRegistrant['identity'])
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(422)
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
      expect(res.body.errors[0].field).to.eql(test.reqField)
      expect(res.body.errors[0].messages[0]).to.eql('file must be image png')
      if(test.reqField === 'ktp') {
        dataRegistrant[test.reqField] = path.resolve(__dirname, './assets/ktp.png')
      } else if(test.reqField === 'selfie'){
        dataRegistrant[test.reqField] = path.resolve(__dirname, './assets/selfie.png')
      }
      
    })
  })
 })
 testsIdentityNIK.forEach(function(test) {
  it('As a user, I couldn\'t register when identity.nik ' + test.desc, () => {
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', dataRegistrant['email'])
    .field('phone', dataRegistrant['phone'])
    .field('identity', JSON.stringify({ 
      nik: test.fieldValue, 
      nama: faker.name.firstName(), 
      tanggalLahir: '1990-01-01' 
    }))
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(422)
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      if (test.cond === 'blank') {
        expect(res.body.errors[0].messages[0]).to.eql('cannot be blank')
      } else if(test.cond === 'nan') {
        expect(res.body.errors[0].messages[0]).to.eql('is not a number')
      } else if(test.cond === 'less'||'more') {
        expect(res.body.errors[0].messages[0]).to.eql('NIK must be a 16 digit number')
      }
    })
  })
 })
 it('As a user, I couldn\'t register when identity.nama is less than 3character', () => {
  return agent
  .post('/v3/merchant/registration')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('email', dataRegistrant['email'])
  .field('phone', dataRegistrant['phone'])
  .field('identity', JSON.stringify({ 
    nik: '3404082210930002', 
    nama: 'ab', 
    tanggalLahir: '1990-01-01' 
  }))
  .attach('ktp', dataRegistrant['ktp'])
  .attach('selfie', dataRegistrant['selfie'])
  .expect(422)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')  
    expect(res.body.errors[0].messages[0]).to.eql('is too short (minimum is 3 characters)')
  })
 })
})
