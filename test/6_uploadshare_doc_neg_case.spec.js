const path    = require('path')
const expect  = require('chai').expect
const request = require('supertest')
const faker   = require('faker/locale/id_ID')
const config  = require('./utils/config')
const storage = require('./utils/storage')
const error   = require('./utils/error')

const agent = request(config.baseURL)

const uploadJson = {
  documentTitle: "Tes Upload Dokumen " + new Date().toLocaleDateString(),
  docType: "Serial",
  owner: JSON.stringify({ privyId: "TES001", enterpriseToken: "41bc84b42c8543daf448d893c255be1dbdcc722e"}),
  recipients: JSON.stringify([{ privyId: "UAT001", type: "Signer", enterpriseToken: "" },{ privyId: "UAT002", type: "Signer", enterpriseToken: "" }]),
  document: path.resolve(__dirname, './assets/tes.pdf')
}

describe('Negative case for upload document', () => {

  var testsAuth = [
    { username: '', password:'', desc: 'with empty basic auth'},
    { username: 'abcdef', password:'abcdef', desc: 'with invalid/random username password for authorization'}
  ]
  
  var testMerchantKey = [
    { merchantKey: '', desc: 'with empty Merchant-Key'},
    { merchantKey: 'abcdef', desc: 'with invalid/random Merchant-Key'}
  ]

  var testsWithout = [
    { reqField: ['emptyfield', 'docType', 'owner', 'document', 'recipients'], woField: 'documentTitle'},
    { reqField: ['documentTitle', 'emptyfield', 'owner', 'document', 'recipients'], woField: 'docType'},
    { reqField: ['documentTitle', 'docType', 'emptyfield', 'document', 'recipients'], woField: 'owner'},
    { reqField: ['documentTitle', 'docType', 'owner', 'emptyfield', 'recipients'], woField: 'document'},
    { reqField: ['documentTitle', 'docType', 'owner', 'document', 'emptyfield'], woField: 'recipients'}
  ] 

  var testsEmpty = [
    { reqField: 'documentTitle', defValue: uploadJson['documentTitle']},
    { reqField: 'docType', defValue: uploadJson['docType']},
    { reqField: 'owner', defValue: uploadJson['owner']},
    { reqField: 'document', defValue: uploadJson['document']},
    { reqField: 'recipients', defValue: uploadJson['recipients']}
  ] 

  var testsEnterprise = [
    { enterpriseToken: '', cond: 'empty', desc: 'when enterpriseToken is empty'},
    { enterpriseToken: '41bc84b42c8543daf448d893c255be1dbdcc722e', cond: 'invalid', desc: 'with privyid that hasn\'t been association with the enterprise'}
  ]

  testsAuth.forEach(function(test) {
    it('As a user, I couldn\'t upload and share a document '+ test.desc, () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(test.username, test.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .attach('document', uploadJson['document'])
      .expect(401)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message') 
        expect(res.body.message).to.eql('Unauthorized. Invalid username or password.')
      })
    })
  })
  testMerchantKey.forEach(function(test) {
    it('As a user, I couldn\'t upload and share a document '+ test.desc, () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', test.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .attach('document', uploadJson['document'])
      .expect(401)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.message).to.eql('Unauthorized. Invalid Merchant Key.')
      })
    })
  })
  testsWithout.forEach(function(test) {
    it('As a user, I couldn\'t upload a document without ' + test.woField + ' field', () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field(test.reqField[0], uploadJson['documentTitle'])
      .field(test.reqField[1], uploadJson['docType'])
      .field(test.reqField[2], uploadJson['owner'])
      .attach(test.reqField[3], uploadJson['document'])
      .field(test.reqField[4], uploadJson['recipients'])
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      })
    })
  })
  testsEmpty.forEach(function(test) {
    it('As a user, I couldn\'t upload a document with ' + test.reqField + ' field is empty', () => {
      uploadJson[test.reqField] = ''

      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .attach('document', uploadJson['document'])
      .field('recipients', uploadJson['recipients'])
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.errors[0].field).to.eql(test.reqField)
        uploadJson[test.reqField] = test.defValue
      })
    })
  })
  testsEnterprise.forEach(function(test) {
    it('As a user, I couldn\'t upload and share a document '+ test.desc, () => {
      return agent
      .post('/v3.1/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .field('owner', JSON.stringify({ 
        privyId: "NN3772", 
        enterpriseToken: test.enterpriseToken }))
      .attach('document', uploadJson['document'])
      .field('document', uploadJson['recipients'])
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      })
    })
    it('As a user, I couldn\'t upload and share non pdf file in document field', () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .attach('document', path.resolve(__dirname, './assets/ktp.png'))
      .field('recipients', uploadJson['recipients'])
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.errors[0].field).to.eql('document')
        expect(res.body.errors[0].messages[0]).to.eql('Invalid pdf file')
      })
    }) 
    it('As a user, I couldn\'t upload and share when docType value is neither Parallel and Serial', () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', 'Seriall')
      .field('owner', uploadJson['owner'])
      .attach('document', uploadJson['document'])
      .field('recipients', uploadJson['recipients'])
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.errors[0].field).to.eql('docType')
        expect(res.body.errors[0].messages[0]).to.eql('only fill with Serial or Parallel')
      })
    })
    it('As a user, I couldn\'t upload and share with privyid that hasn\'t been association with the enterprise', () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', JSON.stringify({ privyId: "REK013", enterpriseToken: "41bc84b42c8543daf448d893c255be1dbdcc722e" }))
      .attach('document', uploadJson['document'])
      .field('recipients', uploadJson['recipients'])
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.errors[0].field).to.eql('owner.enterpriseToken')
        expect(res.body.errors[0].messages[0]).to.eql('Invalid EnterpriseToken')
      })
    })
    it('As a user, I couldn\'t upload and share when recipients.privyId isn\'t registered', () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .attach('document', uploadJson['document'])
      .field('recipients', JSON.stringify([{ privyId: "XXX001", type: "Signer", enterpriseToken: "" },{ privyId: "UAT002", type: "Signer", enterpriseToken: "" }]))
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      })
    })
    it('As a user, I couldn\'t upload and share when recipients.privyId as both Signer and Reviewer', () => {
      return agent
      .post('/v3/merchant/document/upload')
      .auth(config.username, config.password)
      .set('Merchant-Key', config.merchantKey)
      .field('documentTitle', uploadJson['documentTitle'])
      .field('docType', uploadJson['docType'])
      .field('owner', uploadJson['owner'])
      .attach('document', uploadJson['document'])
      .field('recipients', JSON.stringify([{ privyId: "UAT002", type: "Signer", enterpriseToken: "" },{ privyId: "UAT002", type: "Signer", enterpriseToken: "" }]))
      .expect(422)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
      })
    })
  })

})
