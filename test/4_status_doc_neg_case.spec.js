const path    = require('path')
const expect  = require('chai').expect
const request = require('supertest')
const faker   = require('faker/locale/id_ID')
const config  = require('./utils/config')
const storage = require('./utils/storage')
const error   = require('./utils/error')

const agent = request(config.baseURL)

describe('Negative case for status document', () => {

  var testsAuth = [
    { username: '', password:'', desc: 'with empty basic auth'},
    { username: 'abcdef', password:'abcdef', desc: 'with invalid/random username password for authorization'}
  ]
  
  var testMerchantKey = [
    { merchantKey: '', desc: 'with empty Merchant-Key'},
    { merchantKey: 'abcdef', desc: 'with invalid/random Merchant-Key'}
  ]

  testsAuth.forEach(function(test) {
    it('As a user, I couldn\'t check status document'+ test.desc, () => {
      return agent
      .get('/v3/merchant/document/status/abcdads' )
      .auth(test.username, test.password)
      .set('Merchant-Key', config.merchantKey)
      .expect(401)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.message).to.eql('Unauthorized. Invalid username or password.')
      })
    })
  })

  testMerchantKey.forEach(function(test) {
    it('As a user, I couldn\'t check status document'+ test.desc, () => {
      return agent
      .get('/v3/merchant/document/status/abcdads' )
      .auth(config.username, config.password)
      .set('Merchant-Key', test.merchantKey)
      .expect(401)
      .then((res) => {   
        expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
        expect(res.body.message).to.eql('Unauthorized. Invalid Merchant Key.')
      })
    })
  })
})
