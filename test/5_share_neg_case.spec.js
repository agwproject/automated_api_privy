const path    = require('path')
const expect  = require('chai').expect
const request = require('supertest')
const faker   = require('faker/locale/id_ID')
const config  = require('./utils/config')
const storage = require('./utils/storage')
const error   = require('./utils/error')

const agent = request(config.baseURL)
const phoneHead = ['081', '083', '085','087', '089']

const dataRegistrant = { 
    email: faker.internet.email(), 
    phone: phoneHead[Math.floor(Math.random() * phoneHead.length)] + Math.floor(100000000 + Math.random() * 900000000),
    tokenUserverified: '',
    tokenUserregistered: '',
    tokenUserrejected: '',
    tokenDocumentingprogress: '',
    tokenDocumentcompleted: '',
    ktp: path.resolve(__dirname, './assets/ktp.png'),
    selfie: path.resolve(__dirname, './assets/selfie.png'),
    identity: JSON.stringify({ nik: '3404082210930002', nama: faker.name.firstName(), tanggalLahir: '1990-01-01' })
 }

 describe('Negative case for share document', () => {
 
  var testsAuth = [
   { username: '', password:'', desc: 'with empty basic auth'},
   { username: 'abcdef', password:'abcdef', desc: 'with invalid/random username password for authorization'}
 ]

 var testMerchantKey = [
   { merchantKey: '', desc: 'with empty Merchant-Key'},
   { merchantKey: 'abcdef', desc: 'with invalid/random Merchant-Key'}
 ]

 var testsEmpty = [
   { reqField: 'docToken', defValue: dataRegistrant['docToken']},
   { reqField: 'recipients', defValue: dataRegistrant['recipients']}
 ] 

 var testsWithout = [
   { payload: {'docTokena': storage.get('docToken'),'recipients':[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]}, woField: 'docToken'},
   { payload: {'docToken': storage.get('docToken'),'recipientsa':[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]}, woField: 'recipients'}
 ]

 var testsEmpty = [
   { payload: {'docToken': '','recipients':[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]}, woField: 'docToken'},
   { payload: {'docToken': storage.get('docToken'),'recipients':[]}, woField: 'recipients'}
 ] 

 testsAuth.forEach(function(test) {
  it('As a user, I couldn\'t share '+ test.desc, () => {
     return agent
     .post('/v3.1/merchant/document/share')
     .auth(test.username, test.password)
     .set('Merchant-Key', config.merchantKey)
     .set('Accept', 'application/json')
     .send({"docToken": storage.get('docToken'),"recipients":[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]})
     .expect(401)
     .then((res) => {     
       expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message') 
       expect(res.body.message).to.eql('Unauthorized. Invalid username or password.')
   })
  })
 })
 testMerchantKey.forEach(function(test) {
  it('As a user, I couldn\'t share '+test.desc, () => {
     return agent
     .post('/v3.1/merchant/document/share')
     .auth(config.username, config.password)
     .set('Merchant-Key', test.merchantKey)
     .set('Accept', 'application/json')
     .send({"docToken": storage.get('docToken'),"recipients":[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]})
     .expect(401)
     .then((res) => {
       expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')
       expect(res.body.message).to.eql('Unauthorized. Invalid Merchant Key.')  
     })
  })
 })
 testsWithout.forEach(function(test) {
  it('As a user, I couldn\'t share without field '+ test.woField, function() {
    return agent
    .post('/v3.1/merchant/document/share')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .set('Accept', 'application/json')
    .send(test.payload)
    .expect(422)
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
    })
  })
 })
 testsEmpty.forEach(function(test) {
  it('As a user, I couldn\'t share with value of field '+ test.woField +' empty', function() {
    return agent
    .post('/v3.1/merchant/document/share')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .set('Accept', 'application/json')
    .send(test.payload)
    .expect(422)
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')   
    })
  })
 })
 it('As a user, I couldn\'t share a document that already shared', function() {
  return agent
  .post('/v3.1/merchant/document/share')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .set('Accept', 'application/json')
  .send({'docToken': '8a1bd842e373c10801aa996944b5a743dc06007300719113b80739b1e5990a77','recipients':[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]})
  .expect(422)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')     
    expect(res.body.errors[0].field).to.eql('docToken')
    expect(res.body.errors[0].messages[0]).to.eql('docToken already exists')
  })
 })
 it('As a user, I couldn\'t share document using not verified privyId', function() {
  return agent
  .post('/v3.1/merchant/document/share')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .set('Accept', 'application/json')
  .send({'docToken': 'b430684b3e8b46a4a4e4a5a48c375bb1518e547b8c79f6fcbbeefe78ced34b3d','recipients':[{"privyId":"XXX001","type":"Reviewer"}]})
  .expect(422)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')    
    expect(res.body.errors[0].messages[0]).to.eql('privyId XXX001 not found')
  })
 })
 it('As a user, I couldn\'t share document when not using value Signer or Reviewer', function() {
  return agent
  .post('/v3.1/merchant/document/share')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .set('Accept', 'application/json')
  .send({'docToken': 'b430684b3e8b46a4a4e4a5a48c375bb1518e547b8c79f6fcbbeefe78ced34b3d','recipients':[{"privyId":"UAT001","type":"Other"}]})
  .expect(422)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')    
    expect(res.body.errors[0].messages[0]).to.eql('Only fill with Signer or Reviewer')
  })
 })
 it('As a user, I couldn\'t share other merchant\'s document', function() {
  return agent
  .post('/v3.1/merchant/document/share')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .set('Accept', 'application/json')
  .send({'docToken': '82a7f5faba713ab1f5cc22297635be9c779baf3d45d510ea6d65d54f8279ae23','recipients':[{"privyId":"UAT001","type":"Signer"}]})
  .expect(401)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')    
    expect(res.body.message).to.eql('Unauthorized Document Access. This docToken does not belong to this merchant')
  })
 })
 it('As a user, I couldn\'t share using random doctoken', function() {
  return agent
  .post('/v3.1/merchant/document/share')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .set('Accept', 'application/json')
  .send({'docToken': 'abcdefghijklmnopqrstu','recipients':[{"privyId":"UAT001","type":"Signer"}]})
  .expect(422)
  .then((res) => { 
    expect(res.body).to.be.an('object').to.have.all.keys('code', 'errors', 'message')    
  })
 })
})
