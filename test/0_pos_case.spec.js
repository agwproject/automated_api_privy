const path    = require('path')
const expect  = require('chai').expect
const request = require('supertest')
const faker   = require('faker/locale/id_ID')
const config  = require('./utils/config')
const storage = require('./utils/storage')
const error   = require('./utils/error')

const agent = request(config.baseURL)
const phoneHead = ['081', '083', '085','087', '089']

const dataRegistrant = { 
    email: faker.internet.email(), 
    phone: phoneHead[Math.floor(Math.random() * phoneHead.length)] + Math.floor(100000000 + Math.random() * 900000000),
    tokenUserverified: '',
    tokenUserregistered: '',
    tokenUserrejected: '',
    tokenDocumentingprogress: '',
    tokenDocumentcompleted: '',
    ktp: path.resolve(__dirname, './assets/ktp.png'),
    selfie: path.resolve(__dirname, './assets/selfie.png'),
    identity: JSON.stringify({ nik: '3404082210930002', nama: faker.name.firstName(), tanggalLahir: '1990-01-01' })
 }

describe('API working properly', () => {
 it('As a user, I could register a user', () => {
    return agent
    .post('/v3/merchant/registration')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('email', dataRegistrant['email'])
    .field('phone', dataRegistrant['phone'])
    .field('identity', dataRegistrant['identity'])
    .attach('ktp', dataRegistrant['ktp'])
    .attach('selfie', dataRegistrant['selfie'])
    .expect(201)
    .on('error', error)
    .then((res) => { 
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'data', 'message')     
      expect(res.body.data).to.be.an('object').to.have.all.keys('email', 'phone', 'userToken', 'status')     
      expect(res.body.data.status).to.eql('waiting')     
      expect(res.body.message).to.eql('Waiting for Verification') 
      storage.set('tokenUserWaiting', res.body.data.userToken) 
    })
 })
 it('Check default response status user waiting', () => {
  return agent
  .post('/v3/merchant/registration/status')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('token', storage.get('tokenUserWaiting'))
  .expect(201)
  .on('error', error)
  .then((res) => {
    expect(res.body.code).to.eql(201)     
    expect(res.body.data).to.be.an('object').to.have.all.keys('email', 'phone', 'userToken', 'status')     
    expect(res.body.data.status).to.eql('waiting')     
    expect(res.body.message).to.eql('Waiting for Verification') 
  })
 }) 
 it('Check default response for status user invalid', () => {
  return agent
  .post('/v3/merchant/registration/status')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('token', config.userToken.invalid)
  .expect(201)
  .on('error', error)
  .then((res) => {
    expect(res.body.code).to.eql(201)     
    expect(res.body.data.privyId).to.be.null     
    expect(res.body.data).to.be.an('object').to.have.all.keys('privyId', 'email', 'phone', 'processedAt', 'userToken', 'status', 'reject')     
    expect(res.body.data.reject).to.be.an('object').to.have.all.keys('code', 'reason', 'handlers')     
    expect(res.body.data.status).to.eql('invalid')     
    expect(res.body.message).to.eql('Data Rejected') 
  })
 }) 
 it('Check default response for status user rejected', () => {
  return agent
  .post('/v3/merchant/registration/status')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('token', config.userToken.rejected)
  .expect(201)
  .on('error', error)
  .then((res) => {
    expect(res.body.code).to.eql(201)     
    expect(res.body.data.privyId).to.be.null     
    expect(res.body.data).to.be.an('object').to.have.all.keys('privyId', 'email', 'phone', 'processedAt', 'userToken', 'status', 'reject')     
    expect(res.body.data.reject).to.be.an('object').to.have.all.keys('code', 'reason', 'handlers')     
    expect(res.body.data.status).to.eql('rejected')     
    expect(res.body.message).to.eql('Data Rejected') 
  })
 }) 
 it('Check default response for status user verified', () => {
  return agent
  .post('/v3/merchant/registration/status')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('token', config.userToken.verified)
  .expect(201)
  .on('error', error)
  .then((res) => {
    expect(res.body.code).to.eql(201)     
    expect(res.body.data.privyId).to.not.be.null   
    expect(res.body.data).to.be.an('object').to.have.all.keys('privyId', 'email', 'phone', 'processedAt', 'userToken', 'status', 'identity')
    expect(res.body.data.identity).to.be.an('object').to.have.all.keys('nama', 'nik', 'tanggalLahir', 'tempatLahir')
    expect(res.body.data.status).to.eql('verified')     
    expect(res.body.message).to.eql('Data Verified') 
  })
 }) 
 it('Check default response for status user registered', () => {
  return agent
  .post('/v3/merchant/registration/status')
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .field('token', config.userToken.registered)
  .expect(201)
  .on('error', error)
  .then((res) => {
    expect(res.body.code).to.eql(201)     
    expect(res.body.data.privyId).to.not.be.null   
    expect(res.body.data).to.be.an('object').to.have.all.keys('privyId', 'email', 'phone', 'processedAt', 'userToken', 'status', 'identity')
    expect(res.body.data.identity).to.be.an('object').to.have.all.keys('nama', 'nik', 'tanggalLahir', 'tempatLahir')
    expect(res.body.data.status).to.eql('registered')     
    expect(res.body.message).to.eql('Data has been Registered') 
  })
 })
 it('I could upload a document', () => {
    var d = new Date().toLocaleDateString()
    return agent
    .post('/v3.1/merchant/document/upload')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('documentTitle', "Tes Upload Dokumen " + d)
    .field('docType', "Parallel")
    .field('owner', JSON.stringify({ 
      privyId: "TES001",
      enterpriseToken: "41bc84b42c8543daf448d893c255be1dbdcc722e" 
    }))
    .attach('document', path.resolve(__dirname, './assets/tes.pdf'))
    .expect(201)
    .on('error', error)
    .then((res) => {
      expect(res.body.code).to.eql(201)     
      expect(res.body.data).to.be.an('object').to.have.all.keys('docToken', 'urlDocument')      
      expect(res.body.message).to.eql('Document successfully uploaded')
      storage.set('docToken', res.body.data.docToken)
    })
 })
 it('I could check status a draft document', () => {
  return agent
  .get('/v3/merchant/document/status/'+ storage.get('docToken'))
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .expect(200)
  .on('error', error)
  .then((res) => {     
    expect(res.body.data).to.be.an('object').to.have.all.keys('docToken', 'recipients', 'documentStatus', 'urlDocument')      
    expect(res.body.data.documentStatus).to.eql('Draft')
    expect(res.body.message).to.eql('Successfully get a status document')
  })
 })
 it('I could share a document', () => {
    return agent
    .post('/v3.1/merchant/document/share')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .set('Accept', 'application/json')
    .send({"docToken": storage.get('docToken'),"recipients":[{"privyId":"TE4455","type":"Reviewer"},{"privyId":"UAT001","type":"Signer"}]})
    .expect(201)
    .on('error', error)
    .then((res) => {
      expect(res.body.code).to.eql(201)     
      expect(res.body.data).to.be.an('object').to.have.all.keys('recipients')
      res.body.data.recipients.every(i => 
        expect(i).to.have.all.keys('privyId', 'docToken', 'privyId')
      )
      expect(res.body.message).to.eql('This document has been successfully shared')
    })
 })
 it('I could upload and share a document', () => {
    var d = new Date().toLocaleDateString()
    return agent
    .post('/v3/merchant/document/upload')
    .auth(config.username, config.password)
    .set('Merchant-Key', config.merchantKey)
    .field('documentTitle', "Tes Upload Dokumen")
    .field('docType', "Parallel")
    .field('owner', JSON.stringify({ 
      privyId: "TES001",
      enterpriseToken: "41bc84b42c8543daf448d893c255be1dbdcc722e" 
    }))
    .field('recipients', JSON.stringify([{ 
      privyId: "UAT001",
      type: "Signer",
      enterpriseToken: "" 
    },{
      privyId: "UAT002",
      type: "Signer",
      enterpriseToken: "" 
    }]))
    .attach('document', path.resolve(__dirname, './assets/tes.pdf'))
    .expect(201)
    .on('error', error)
    .then((res) => {    
      expect(res.body).to.be.an('object').to.have.all.keys('code', 'data', 'message')
      expect(res.body.data).to.be.an('object').to.have.all.keys('recipients', 'docToken', 'urlDocument')
      res.body.data.recipients.every(i => 
        expect(i).to.have.all.keys('privyId', 'enterpriseToken', 'type')
      )
      expect(res.body.message).to.eql('Document successfully upload and shared')
    })
 })
 it('I could check status a in progress document', () => {
  return agent
  .get('/v3/merchant/document/status/'+ storage.get('docToken'))
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .expect(200)
  .on('error', error)
  .then((res) => {     
    expect(res.body.data).to.be.an('object').to.have.all.keys('docToken', 'recipients','documentStatus', 'urlDocument')      
    expect(res.body.data.documentStatus).to.eql('In Progress')
    res.body.data.recipients.every(i => 
      expect(i).to.have.all.keys('privyId', 'type', 'signatoryStatus')
    )
    expect(res.body.message).to.eql('Successfully get a status document')
  })
 })
 it('I could check status a completed document', () => {
  return agent
  .get('/v3/merchant/document/status/'+ config.docToken.completed)
  .auth(config.username, config.password)
  .set('Merchant-Key', config.merchantKey)
  .expect(200)
  .on('error', error)
  .then((res) => {     
    expect(res.body.data).to.be.an('object').to.have.all.keys('docToken', 'recipients', 'download', 'documentStatus', 'urlDocument')      
    expect(res.body.data.documentStatus).to.eql('Completed')
    res.body.data.recipients.every(i => 
      expect(i).to.have.all.keys('privyId', 'type', 'signatoryStatus')
    )
    expect(res.body.message).to.eql('Successfully get a status document')
  })
 })
})